# moodpoll
App for **easy and good decision making**.

<!-- Note: This file contains some comment-markers which enable the reusage of the text-content at the testing instance  -->
<!-- marker_1 -->

*Easy* because creating a poll and taking part is fast and self-explaining.

*Good* because it becomes clear, which options raise the most support and which raise the most resistance inside the group. This insight enables wiser decisions and more focused discussions.

<!-- marker_2 -->

Testing instance: [moodpoll.uber.space/](https://moodpoll.uber.space/).

**This repository is unmaintained.**  
Both of us maintainers have, after serious consideration, decided to not further pursue this project at this point in time.
We do not have the resources at hand to maintain moodpoll, much less to add features.
The testing instance linked above will stay online for the foreseeable future.
If you are able to take over this project, please get in touch.


## Challenge

A group of people wants to agree on some question with more than one possible option. Naively, anybody has one or more votes, and the option with the most votes is chosen. However, there are situations when this leads to suboptimal results, e.g. if two similar options "compete" for votes letting a third one win which objectively is not wanted by most people.

## General Solution

In many situations "systemic consenting" (originally in German: ["Systemisches Konsensieren"](https://partizipation.at/methoden/systemisches-konsensieren/)) is a better principle. It tries to find the option which raises the least resistance inside the group, and not the one which has the biggest homogeneous subgroup of supporters. Unfortunately it is not yet well known and deserves some explanation.

## Contribution of the App `moodpoll`

The app enables to create a poll within a few seconds. Just type in some text. Every new line is a new option. Users can then vote by using a range-slider and thus express a differentiated opinion between maximum rejection and maximum approval. The result is also differentiated: rejecting, neutral and supporting votes are displayed separately for each option. This allows the group to apply the decision scheme which fits best for the situation at hand. Having the concrete resistance values available, it is, however, much more intuitive to apply systemic consenting. It is not even necessary to explain it beforehand. It will probably explain itself. Once the method has proven useful, it can be introduced formally much easier.


## Very Simple Example:

Say Alice, Bob and Carl want to agree when they meet. Available options are:

1. Monday
1. Tuesday

Alice and Bob have preferences for Monday and are indifferent for Tuesday. Carl prefers Tuesday and is not available on Monday.

Counting only positive votes, Monday would win but exclude one Person. However, Tuesday raises the the least resistance. It is OK for everyone.

While in this situation (few voters, few options) "ordinary communication" would perfectly work, such communication can become quite tedious. As time and willingness to communicate constructively are precious resources, they should not be wasted.

<!-- marker_3 -->


## Current status

The app is currently in *minimum viable product*-state. It should be applicable for the basic use case but obviously is not finished.

For planned but yet missing features see the [list of issues](https://codeberg.org/cknoll/django-moodpoll/issues)


## Demo Videos

### In English
(yet nothing available)

### In German

- https://video.dresden.network/w/429ac760-8d7a-4be4-a7bc-aa840d928ff3 (2021)
- https://y.com.sb/watch?v=nHjb7UJhWPA (2022)

